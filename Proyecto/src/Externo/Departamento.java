package Externo;

/**
 * Created by Erick on 13/06/2017.
 */
public enum Departamento {

    FINANZAS, ADMINISTRACION, MARKETING, PRODUCCION, RRHH, VENTAS;
    int numEmpleados;

    Departamento() {
    }

    Departamento(int numEmpleados) {
        this.numEmpleados = numEmpleados;
    }
}
