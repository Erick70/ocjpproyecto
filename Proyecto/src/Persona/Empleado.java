package Persona;

/**
 * Created by Erick on 13/06/2017.
 */
public abstract class Empleado extends Persona {

    String Departamento;

    public void setDepartamento(String departamento) {
        Departamento = departamento;
    }

    abstract public double calcularSueldo ();
}
