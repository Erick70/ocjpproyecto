package Persona;

/**
 * Created by Erick on 13/06/2017.
 */
public class EmpTiempoCompleto extends Empleado {
    private Desempeño evaluacion ;

    public double getPrecioHorasExtra() {
        return precioHorasExtra;
    }

    public void setPrecioHorasExtra(double precioHorasExtra) {
        this.precioHorasExtra = precioHorasExtra;
    }

    private double precioHorasExtra ;
    public int getHorasTrabajadas() {
        return horasTrabajadas;
    }

    public void setHorasTrabajadas(int horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }

    private int horasTrabajadas;
    public double getSueldoFijo() {
        return sueldoFijo;
    }

    public void setSueldoFijo(double sueldoFijo) {
        this.sueldoFijo = sueldoFijo;
    }

    public double getBonificaciones() {
        return bonificaciones;
    }

    public void setBonificaciones(double bonificaciones) {
        this.bonificaciones = bonificaciones;
    }

    private double sueldoFijo;
    private double bonificaciones;


    @Override
    public double calcularSueldo() {
        return sueldoFijo+bonificaciones;
    }


public double   calcularBonificaciones (){
    if (horasTrabajadas>8){

        bonificaciones = (horasTrabajadas-8)*precioHorasExtra;
    }

    return bonificaciones;
}

public void calcularDesempeño (){

    if (horasTrabajadas<8){
        evaluacion = Desempeño.Malo;
    }
    if (horasTrabajadas==8){
        evaluacion= Desempeño.Regular;
    }
    if (horasTrabajadas>8){
        evaluacion= Desempeño.Bueno;
    }
}


}


