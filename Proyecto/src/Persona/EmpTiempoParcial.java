package Persona;

/**
 * Created by Erick on 13/06/2017.
 */
public class EmpTiempoParcial extends Empleado {
    private int numeroHoras;

    public int getNumeroHoras() {
        return numeroHoras;
    }

    public void setNumeroHoras(int numeroHoras) {
        this.numeroHoras = numeroHoras;
    }

    public double getPrecioHora() {
        return precioHora;
    }

    public void setPrecioHora(double precioHora) {
        this.precioHora = precioHora;
    }

    private double precioHora;

    @Override
    public double calcularSueldo() {
        return numeroHoras*precioHora;
    }
}
