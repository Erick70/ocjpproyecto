package Persona;

import javax.swing.*;


/**
 * Created by Erick on 13/06/2017.
 */
public class Administrador extends Persona {


    private double sueldo;


    public Administrador() {
    }

    public Administrador(double sue ) {
        sueldo =  sue;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    public void setSueldo() {
        JOptionPane.showInputDialog("Ingrese el sueldo del administrador");
    }

    public void asignarEmpleadoADep(Empleado emp, String Depar){

        emp.setDepartamento(Depar);

    }

    @Override
    public String toString() {
        return super.toString() + "\nSueldo =" + sueldo;
    }

}
