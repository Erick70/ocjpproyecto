package com.company;

import com.company.Externo.CalculoImpuestos;
import com.company.Externo.Departamento;
import com.company.Persona.Administrador;
import com.company.Persona.*;
import jdk.nashorn.internal.scripts.JO;

import javax.swing.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static com.company.Externo.Departamento.FINANZAS;
import static com.company.Persona.Administrador.*;


public class Main {

    public static void main(String[] args) {
        HashMap<String, Double> tablaSueldos = new HashMap<String, Double>();
        Boolean flag = false;
        String nombbreEmp, tipoEmpleado, imprirmiSueldos;
        double precioHora;
        double sueldo;
        int horasTrabajadas, horasExtras, numeroEmpleados;


        try {
            numeroEmpleados = Integer.valueOf(JOptionPane.showInputDialog(null, "Cuantos empleados va a ingresar ?"));

            for (int i = 0; i < numeroEmpleados; i++) {
                nombbreEmp = JOptionPane.showInputDialog(null, "Ingrese el nombre del empleado");

                tipoEmpleado = JOptionPane.showInputDialog(null, "Ingrese el tipo de empleado(completo/parcial)");
                if (tipoEmpleado.equals("completo")) {
                    horasTrabajadas = Integer.valueOf(JOptionPane.showInputDialog(null, "Ingrese las horas trabajas"));
                    horasExtras = Integer.valueOf(JOptionPane.showInputDialog(null, "Ingrese las horas extra"));
                    EmpTiempoCompleto empleadoComp = new EmpTiempoCompleto();
                    empleadoComp.setPrecioHorasExtra(12.0);
                    empleadoComp.setPrecioHoras(8.0);

                    empleadoComp.setBonificaciones(0);
                    empleadoComp.setHorasExtra(horasExtras);
                    empleadoComp.setHorasTrabajadas(horasTrabajadas);

                    System.out.println("El nombre del empleado es : " + nombbreEmp);
                    System.out.println("El las horas trabajadas son : " + horasTrabajadas);
                    System.out.println("El las horas extras son : " + horasExtras);
                    System.out.println("El sueldo calculado es : " + empleadoComp.calcularSueldo());
                    tablaSueldos.put(nombbreEmp, empleadoComp.calcularSueldo());

                    CalculoImpuestos h1 = new CalculoImpuestos();
                    h1.setName("Hilo 1");

                    CalculoImpuestos h2 = new CalculoImpuestos();
                    h1.setName("Hilo 2");

                    h1.start();
                    h2.start();
                }
                if (tipoEmpleado.equals("parcial")) {
                    horasTrabajadas = Integer.valueOf(JOptionPane.showInputDialog(null, "Ingrese las horas trabajas"));
                    precioHora = Double.valueOf(JOptionPane.showInputDialog(null, "Ingrese el precio de la hora"));
                    EmpTiempoParcial empleadoParc = new EmpTiempoParcial();

                    empleadoParc.setNumeroHoras(horasTrabajadas);
                    empleadoParc.setPrecioHora(precioHora);


                    System.out.println("El nombre del empleado es : " + nombbreEmp);
                    System.out.println("El las horas trabajadas son : " + horasTrabajadas);
                    System.out.println("El sueldo calculado es : " + empleadoParc.calcularSueldo());
                    tablaSueldos.put(nombbreEmp, empleadoParc.calcularSueldo());


                    CalculoImpuestos h1 = new CalculoImpuestos();
                    h1.setName("Hilo 1");

                    CalculoImpuestos h2 = new CalculoImpuestos();
                    h1.setName("Hilo 2");

                    h1.start();
                    h2.start();
                }
                if (!tipoEmpleado.equals("completo") && !tipoEmpleado.equals("parcial")) {
                    JOptionPane.showMessageDialog(null, "Tipo de empleado incorrecto, ingrese 'completo' o 'parcial'");
                    flag = true;
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Datos ingresados incorrctos");
        }

        imprirmiSueldos = JOptionPane.showInputDialog(null, "Desea imprimir tabla de sueldos");

        if (imprirmiSueldos.equals("si")) {
            Iterator it = tablaSueldos.entrySet().iterator();
            System.out.println("TABLA DE SUELDOS");
            System.out.println("NOMBRE\tSUELDO");
            while (it.hasNext()) {
                Map.Entry e = (Map.Entry) it.next();
                System.out.println(e.getKey() + "\t" + e.getValue());
            }
        }
        if (imprirmiSueldos.equals("no")) {

        }
        if (!imprirmiSueldos.equals("si") && !imprirmiSueldos.equals("no")) {
            JOptionPane.showMessageDialog(null, "Solo puede ingresar 'SI' o 'NO'");
        }

        //Clases Inner

        Administrador admin = new Administrador();
        admin.setSueldo(1200);

        EmpTiempoCompleto emTC = new EmpTiempoCompleto();
        emTC.setNombre(JOptionPane.showInputDialog("Ahora es Empleado\nIngrese el nombre del empleado"));
        emTC.setHorasTrabajadas(40);
        emTC.setSueldoFijo(1100);
        emTC.infoEmpleado();

        Cocinero coci = new Cocinero() {

            public void preparaRefrigerio(Empleado e) {
                System.out.println("Ahora es cocinero\nPrepara postres para " + e.getNombre());
            }
        };
        coci.preparaRefrigerio(emTC);
    }
}
