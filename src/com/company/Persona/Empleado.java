package com.company.Persona;


public abstract class Empleado extends Persona implements Comunicador{
    String Departamento;

    public void setDepartamento(String departamento) {
        Departamento = departamento;
    }

    abstract public double calcularSueldo ();

}
