package com.company.Persona;

/**
 * Created by brayan.jimenez on 14/06/2017.
 */
public enum Desempeño {
    Bueno,
    Regular,
    Malo;
}
