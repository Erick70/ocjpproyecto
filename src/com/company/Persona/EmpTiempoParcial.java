package com.company.Persona;

/**
 * Created by brayan.jimenez on 14/06/2017.
 */
public class EmpTiempoParcial extends Empleado {
    static private int numeroHoras;

    public int getNumeroHoras() {
        return numeroHoras;
    }

    public void setNumeroHoras(int numeroHoras) {
        this.numeroHoras = numeroHoras;
    }

    public double getPrecioHora() {
        return precioHora;
    }

    public void setPrecioHora(double precioHora) {
        this.precioHora = precioHora;
    }

    static private double precioHora;

    @Override
    public double calcularSueldo() {
        return numeroHoras*precioHora;
    }

    @Override
    public void DarMensaje() {

    }

    @Override
    public void RecibirMensaje() {

    }
}
