package com.company.Persona;

import javax.swing.*;

/**
 * Created by brayan.jimenez on 14/06/2017.
 */
public class Administrador extends Persona implements Comunicador{
    private double sueldo;
    private String mensaje = "Mensaje";


    public Administrador() {
    }

    public Administrador(double sue ) {
        sueldo =  sue;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
        soloAdmin sA = new soloAdmin();
        sA.mostrarSueldo();
    }

    public void setSueldo() {
        JOptionPane.showInputDialog("Ingrese el sueldo del administrador");
    }

    public void asignarEmpleadoADep(Empleado emp, String Depar){

        emp.setDepartamento(Depar);

    }

    @Override
    public String toString() {
        return super.toString() + "\nSueldo =" + sueldo;
    }

    @Override
    public void DarMensaje() {
        System.out.print("Enviar Mensaje: "+ mensaje);
    }

    @Override
    public void RecibirMensaje() {
        System.out.print("Recibir mensaje: "+ mensaje);
    }

    class soloAdmin {

        public void mostrarSueldo() {
            String resp = JOptionPane.showInputDialog("MUESTRA INNER CLASS\nAhora es administrador\nQuiere mostrar su sueldo(si/no)");
            if (resp.equalsIgnoreCase("si") ){

                System.out.println("El adminsitrador recibe "+ sueldo);
            }
        }
    }


}
