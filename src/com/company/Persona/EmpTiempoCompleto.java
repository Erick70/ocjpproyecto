package com.company.Persona;

/**
 * Created by brayan.jimenez on 14/06/2017.
 */
public class EmpTiempoCompleto extends Empleado {

    private Desempeño evaluacion ;
    static private double precioHoras;

    public double getPrecioHoras() {
        return precioHoras;
    }

    public void setPrecioHoras(double precioHoras) {
        this.precioHoras = precioHoras;
    }

    public int getHorasExtra() {
        return horasExtra;
    }

    public void setHorasExtra(int horasExtra) {
        this.horasExtra = horasExtra;
    }

    static private int horasExtra;


    public double getPrecioHorasExtra() {
        return precioHorasExtra;
    }

    public void setPrecioHorasExtra(double precioHorasExtra) {
        this.precioHorasExtra = precioHorasExtra;
    }

    static private double precioHorasExtra ;
    public int getHorasTrabajadas() {
        return horasTrabajadas;
    }

    public void setHorasTrabajadas(int horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }

    static private int horasTrabajadas;
    public double getSueldoFijo() {
        return sueldoFijo;
    }

    public void setSueldoFijo(double sueldoFijo) {
        this.sueldoFijo = sueldoFijo;
    }

    public double getBonificaciones() {
        return bonificaciones;
    }

    public void setBonificaciones(double bonificaciones) {
        this.bonificaciones = bonificaciones;
    }

    static private double sueldoFijo;
    static private double bonificaciones;


    @Override
    public double calcularSueldo() {
        return (horasTrabajadas* precioHoras)+(horasExtra*precioHorasExtra)+bonificaciones;
    }

    public double   calcularBonificaciones (){
        if (horasTrabajadas>8){

            bonificaciones = (horasTrabajadas-8)*precioHorasExtra;
        }

        return bonificaciones;
    }

    public void calcularDesempeño (){

        if (horasTrabajadas<8){
            evaluacion = Desempeño.Malo;
        }
        if (horasTrabajadas==8){
            evaluacion= Desempeño.Regular;
        }
        if (horasTrabajadas>8){
            evaluacion= Desempeño.Bueno;
        }
    }


    @Override
    public void DarMensaje() {

    }

    @Override
    public void RecibirMensaje() {

    }

    public void infoEmpleado() {
        class interEmpelado {
            void imprimir() {
                System.out.println("Soy un empleado Tiempo completo\nTrabajo: "+ getHorasTrabajadas() + "horas\nmi sueldo es: "+sueldoFijo);
            }
        }

        interEmpelado iE = new interEmpelado();
        iE.imprimir();
    }
}
