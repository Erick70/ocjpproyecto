package com.company.Externo;

/**
 * Created by brayan.jimenez on 14/06/2017.
 */
public enum  Departamento {
    FINANZAS, ADMINISTRACION, MARKETING, PRODUCCION, RRHH, VENTAS;
    int numEmpleados;

    Departamento() {
    }

    Departamento(int numEmpleados) {
        this.numEmpleados = numEmpleados;
    }
}
