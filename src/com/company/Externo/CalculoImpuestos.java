package com.company.Externo;

import com.company.Persona.EmpTiempoParcial;

/**
 * Created by Erick on 16/08/2017.
 */
public class CalculoImpuestos extends Thread{


    double iva = 0.12;
    double salida;
    Calculadora calc  = new Calculadora();
    EmpTiempoParcial empleadoParc = new EmpTiempoParcial();


    public void run()
    {
        salida = calc.multiplicacion(empleadoParc.calcularSueldo(),iva);

        System.out.println("Comenzado por "+ Thread.currentThread().getName()+"El IVA es "+ salida);
    }
};

