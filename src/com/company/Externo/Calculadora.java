package com.company.Externo;

/**
 * Created by brayan.jimenez on 14/06/2017.
 */
public class Calculadora {
    public double suma(double num1, double num2){
        Double doub1;
        Double doub2;
        doub1 = num1;
        doub2 = num2;

        return doub1 + doub2;
    }

    public double resta(double num1, double num2){
        Double doub1;
        Double doub2;
        doub1 = num1;
        doub2 = num2;

        return doub1 - doub2;
    }

    public double multiplicacion(double num1, double num2){
        Double doub1;
        Double doub2;
        doub1 = num1;
        doub2 = num2;

        return doub1 * doub2;

    }

    public double division(int ent, int ent1){
        double  num1 = ent;
        double num2 = ent1;

        testAssert(num2);


        double comand;
        try{
            comand = (double)num1 / num2;
        }
        catch (Exception e){
            System.out.println("error");
            return 0;

        }
        return comand;
    }

    public double modulo(double num1, double num2){
        Double doub1;
        Double doub2;
        doub1 = num1;
        doub2 = num2;

        return doub1 % doub2;
    }

    public void testAssert(double num1)
    {
        assert num1<=0 : "No existe división para 0";
    }

}
